#include "MyApp.h"
#include "GLUtils.hpp"

#include <math.h>

CMyApp::CMyApp(void)
{
	m_vaoID = 0;
	m_vboID = 0;
	m_ibID = 0;

	m_programID = 0;
}


CMyApp::~CMyApp(void)
{
}

//
// egy parametrikus fel�let (u,v) param�ter�rt�kekhez tartoz� pontj�nak
// kisz�m�t�s�t v�gz� f�ggv�ny
//
glm::vec3	CMyApp::GetUV(float u, float v)
{
	// orig� k�z�ppont�, egys�gsugar� g�mb parametrikus alakja: http://hu.wikipedia.org/wiki/G%C3%B6mb#Egyenletek 
	// figyelj�nk:	matematik�ban sokszor a Z tengely mutat felfel�, de n�lunk az Y, teh�t a legt�bb k�plethez k�pest n�lunk
	//				az Y �s Z koordin�t�k felcser�lve szerepelnek
	u *= 2*3.1415f;
	v *= 3.1415f;
	float cu = cosf(u), su = sinf(u), cv = cosf(v), sv = sinf(v);
	float r = 2;

	return glm::vec3( r*cu*sv, r*cv, r*su*sv );
}

void CMyApp::MakeCylinder(float base_radius, float height, int base_vertex_num, std::vector<Vertex>& verts, std::vector<GLushort>& indices) {
	verts.clear();
	indices.clear();
	verts.resize(base_vertex_num * 2);
	float step = glm::two_pi<float>() / (float)base_vertex_num;

	//k�t k�rlap cs�cspontjai
	for (int i = 0; i < base_vertex_num; ++i) {
		glm::vec3 pos1 = { base_radius * cosf(step * (float)i), 0.0f, base_radius * sinf(step * (float)i) };
		glm::vec3 pos2 = pos1 + glm::vec3(0.0f, height, 0.0f);
		verts[i] = Vertex{ pos1, glm::normalize(pos1) - glm::vec3(0.0f, 0.2f, 0.0f) };
		verts[i + base_vertex_num] = Vertex{ pos2, glm::normalize(pos2) - glm::vec3(0.0f, 0.2f, 0.0f) };
	}
	
	//als� lap k�zepe
	glm::vec3 bottom_center = { 0.0f, 0.0f, 0.0f };
	//fels� lap k�zepe
	glm::vec3 top_center = bottom_center + glm::vec3(0.0f, height, 0.0f);
	verts.push_back(Vertex{ bottom_center, glm::vec3(1.0f, 1.0f, 0.0f) });
	verts.push_back(Vertex{ top_center, glm::vec3(1.0f, 1.0f, 0.0f) });

	for (int i = 0; i < base_vertex_num; ++i) {
		//fels� lap
		indices.push_back(base_vertex_num * 2 + 1);
		indices.push_back(base_vertex_num + (i + 1) % base_vertex_num);
		indices.push_back(base_vertex_num + i);
		//els� h�romsz�g
		indices.push_back(i);
		indices.push_back(base_vertex_num + i);
		indices.push_back((i + 1) % base_vertex_num);
		//m�sodik h�romsz�g
		indices.push_back(base_vertex_num + i);
		indices.push_back(base_vertex_num + (i + 1) % base_vertex_num);
		indices.push_back((i + 1) % base_vertex_num);
		//als� lap
		indices.push_back(base_vertex_num * 2 + 0);
		indices.push_back(i);
		indices.push_back((i + 1) % base_vertex_num);
	}
}

bool CMyApp::Init()
{
	// t�rl�si sz�n legyen k�kes
	glClearColor(0.125f, 0.25f, 0.5f, 1.0f);

	glEnable(GL_CULL_FACE); // kapcsoljuk be a hatrafele nezo lapok eldobasat
	glEnable(GL_DEPTH_TEST); // m�lys�gi teszt bekapcsol�sa (takar�s)
	glCullFace(GL_BACK); // GL_BACK: a kamer�t�l "elfel�" n�z� lapok, GL_FRONT: a kamera fel� n�z� lapok

	//
	// geometria letrehozasa
	//


	MakeCylinder(1.0f, 3.0f, 30, m_cylinder_verts, m_cylinder_indices);

	// 1 db VAO foglalasa
	glGenVertexArrays(1, &m_vaoID);
	// a frissen gener�lt VAO beallitasa akt�vnak
	glBindVertexArray(m_vaoID);
	
	// hozzunk l�tre egy �j VBO er�forr�s nevet
	glGenBuffers(1, &m_vboID); 
	glBindBuffer(GL_ARRAY_BUFFER, m_vboID); // tegy�k "akt�vv�" a l�trehozott VBO-t
	// t�lts�k fel adatokkal az akt�v VBO-t
	glBufferData( GL_ARRAY_BUFFER,	// az akt�v VBO-ba t�lts�nk adatokat
				  m_cylinder_verts.size() * sizeof(Vertex),		// ennyi b�jt nagys�gban
				  m_cylinder_verts.data(),	// err�l a rendszermem�riabeli c�mr�l olvasva
				  GL_STATIC_DRAW);	// �gy, hogy a VBO-nkba nem tervez�nk ezut�n �rni �s minden kirajzol�skor felhasnz�ljuk a benne l�v� adatokat
	

	// VAO-ban jegyezz�k fel, hogy a VBO-ban az els� 3 float sizeof(Vertex)-enk�nt lesz az els� attrib�tum (poz�ci�)
	glEnableVertexAttribArray(0); // ez lesz majd a poz�ci�
	glVertexAttribPointer(
		0,				// a VB-ben tal�lhat� adatok k�z�l a 0. "index�" attrib�tumait �ll�tjuk be
		3,				// komponens szam
		GL_FLOAT,		// adatok tipusa
		GL_FALSE,		// normalizalt legyen-e
		sizeof(Vertex),	// stride (0=egymas utan)
		0				// a 0. index� attrib�tum hol kezd�dik a sizeof(Vertex)-nyi ter�leten bel�l
	); 

	// a m�sodik attrib�tumhoz pedig a VBO-ban sizeof(Vertex) ugr�s ut�n sizeof(glm::vec3)-nyit menve �jabb 3 float adatot tal�lunk (sz�n)
	glEnableVertexAttribArray(1); // ez lesz majd a sz�n
	glVertexAttribPointer(
		1,
		3, 
		GL_FLOAT,
		GL_FALSE,
		sizeof(Vertex),
		(void*)(sizeof(glm::vec3)) );

	// index puffer l�trehoz�sa
	glGenBuffers(1, &m_ibID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_cylinder_indices.size() * sizeof(GLushort), m_cylinder_indices.data(), GL_STATIC_DRAW);

	glBindVertexArray(0); // felt�lt�tt�k a VAO-t, kapcsoljuk le
	glBindBuffer(GL_ARRAY_BUFFER, 0); // felt�lt�tt�k a VBO-t is, ezt is vegy�k le
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0); // felt�lt�tt�k a VBO-t is, ezt is vegy�k le

	//
	// shaderek bet�lt�se
	//
	GLuint vs_ID = loadShader(GL_VERTEX_SHADER,		"myVert.vert");
	GLuint fs_ID = loadShader(GL_FRAGMENT_SHADER,	"myFrag.frag");

	// a shadereket t�rol� program l�trehoz�sa
	m_programID = glCreateProgram();

	// adjuk hozz� a programhoz a shadereket
	glAttachShader(m_programID, vs_ID);
	glAttachShader(m_programID, fs_ID);

	// VAO-beli attrib�tumok hozz�rendel�se a shader v�ltoz�khoz
	// FONTOS: linkel�s el�tt kell ezt megtenni!
	glBindAttribLocation(	m_programID,	// shader azonos�t�ja, amib�l egy v�ltoz�hoz szeretn�nk hozz�rendel�st csin�lni
							0,				// a VAO-beli azonos�t� index
							"vs_in_pos");	// a shader-beli v�ltoz�n�v
	glBindAttribLocation( m_programID, 1, "vs_in_col");

	// illessz�k �ssze a shadereket (kimen�-bemen� v�ltoz�k �sszerendel�se stb.)
	glLinkProgram(m_programID);

	// linkeles ellenorzese
	GLint infoLogLength = 0, result = 0;

	glGetProgramiv(m_programID, GL_LINK_STATUS, &result);
	glGetProgramiv(m_programID, GL_INFO_LOG_LENGTH, &infoLogLength);
	if ( GL_FALSE == result )
	{
		std::vector<char> ProgramErrorMessage( infoLogLength );
		glGetProgramInfoLog(m_programID, infoLogLength, NULL, &ProgramErrorMessage[0]);
		fprintf(stdout, "%s\n", &ProgramErrorMessage[0]);
		
		char* aSzoveg = new char[ProgramErrorMessage.size()];
		memcpy( aSzoveg, &ProgramErrorMessage[0], ProgramErrorMessage.size());

		std::cout << "[app.Init()] S�der Huba panasza: " << aSzoveg << std::endl;

		delete aSzoveg;
	}

	// mar nincs ezekre szukseg
	glDeleteShader( vs_ID );
	glDeleteShader( fs_ID );

	//
	// egy�b inicializ�l�s
	//

	// vet�t�si m�trix l�trehoz�sa
	m_matProj = glm::perspective( 45.0f, 640/480.0f, 1.0f, 1000.0f );

	// shader-beli transzform�ci�s m�trixok c�m�nek lek�rdez�se
	m_loc_mvp = glGetUniformLocation( m_programID, "MVP");

	m_loc_curr_col = glGetUniformLocation(m_programID, "curr_col");
	m_loc_f        = glGetUniformLocation(m_programID, "f");
	m_loc_key	   = glGetUniformLocation(m_programID, "key");

	return true;
}

void CMyApp::Clean()
{
	glDeleteBuffers(1, &m_vboID);
	glDeleteBuffers(1, &m_ibID);
	glDeleteVertexArrays(1, &m_vaoID);

	glDeleteProgram( m_programID );
}

void CMyApp::Update()
{
	m_f = abs(sinf(glm::pi<float>() * SDL_GetTicks() / 5000.0f));

	m_matView = glm::lookAt(glm::vec3( 15.0f, 3.0f, 15.0f),		
							glm::vec3( 0,  2,  0),
							glm::vec3( 0,  1,  0));
}


void CMyApp::Render()
{
	// t�r�lj�k a frampuffert (GL_COLOR_BUFFER_BIT) �s a m�lys�gi Z puffert (GL_DEPTH_BUFFER_BIT)
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// shader bekapcsolasa
	glUseProgram( m_programID );

	// shader parameterek be�ll�t�sa
	/*

	GLM transzform�ci�s m�trixokra p�ld�k:
		glm::rotate<float>( sz�g, glm::vec3(tengely_x, tengely_y, tengely_z) ) <- tengely_{xyz} k�r�li elforgat�s
		glm::translate<float>( glm::vec3(eltol_x, eltol_y, eltol_z) ) <- eltol�s
		glm::scale<float>( glm::vec3(s_x, s_y, s_z) ) <- l�pt�kez�s

	*/

	float t = SDL_GetTicks() / 7000.0f;

	glm::mat4 m_rotWorld = glm::translate(glm::vec3(0.0f, -cosf(glm::two_pi<float>() * t) * 7.0f, sinf(glm::two_pi<float>() * t) * 7.0f));

	glUniform3f(m_loc_curr_col, m_curr_col.x, m_curr_col.y, m_curr_col.z);
	glUniform1f(m_loc_f, m_f);
	glUniform1f(m_loc_key, m_key);

	for (int i = 0; i < 2; ++i) {
		for (int j = 0; j < 2; ++j) {
			m_matWorld = m_rotWorld * glm::translate(glm::vec3(2.0f * pow(-1.0f, i), 0.0f, 2.0f * pow(-1.0f, j)));

			glm::mat4 mvp = m_matProj * m_matView * m_matWorld;

			// majd k�ldj�k �t a megfelel� m�trixot!
			glUniformMatrix4fv(m_loc_mvp,// erre a helyre t�lts�nk �t adatot
				1,			// egy darab m�trixot
				GL_FALSE,	// NEM transzpon�lva
				&(mvp[0][0])); // innen olvasva a 16 x sizeof(float)-nyi adatot

			// kapcsoljuk be a VAO-t (a VBO j�n vele egy�tt)
			glBindVertexArray(m_vaoID);

			// kirajzol�s
			glDrawElements(GL_TRIANGLES,		// primit�v t�pus
				m_cylinder_indices.size(),		// hany csucspontot hasznalunk a kirajzolashoz
				GL_UNSIGNED_SHORT,	// indexek tipusa
				0);
		}
	}

	for (int i = 0; i < 2; ++i) {
		//K�RD�S: az als� henger alj�t az als� cs�cshoz rakva egym�sba �rnek. Hol legyen az als� henger�nk alja? (itt mindkett�t 1.5f-el feljebb/lentebb toltam, a k�zep�k van az okta�der cs�cs�ban)

		m_matWorld = m_rotWorld * glm::translate(glm::vec3(0.0f, sqrtf(2.0f) * pow(-1.0f, i) + pow(-1.0f, i) * 1.5f, 0.0f));

		glm::mat4 mvp = m_matProj * m_matView * m_matWorld;

		// majd k�ldj�k �t a megfelel� m�trixot!
		glUniformMatrix4fv(m_loc_mvp,// erre a helyre t�lts�nk �t adatot
			1,			// egy darab m�trixot
			GL_FALSE,	// NEM transzpon�lva
			&(mvp[0][0])); // innen olvasva a 16 x sizeof(float)-nyi adatot

		// kapcsoljuk be a VAO-t (a VBO j�n vele egy�tt)
		glBindVertexArray(m_vaoID);

		// kirajzol�s
		glDrawElements(GL_TRIANGLES,		// primit�v t�pus
			m_cylinder_indices.size(),		// hany csucspontot hasznalunk a kirajzolashoz
			GL_UNSIGNED_SHORT,	// indexek tipusa
			0);
	}

	/*
	{
		//KOORDIN�TARENDSZER

		{
			//X tengely
			m_matWorld = glm::translate(glm::vec3(3.0f, 0.0f, 0.0f)) * glm::scale(glm::vec3(3.0f, 0.04f, 0.06f));

			glm::mat4 mvp = m_matProj * m_matView * m_matWorld;

			// majd k�ldj�k �t a megfelel� m�trixot!
			glUniformMatrix4fv(m_loc_mvp,// erre a helyre t�lts�nk �t adatot
				1,			// egy darab m�trixot
				GL_FALSE,	// NEM transzpon�lva
				&(mvp[0][0])); // innen olvasva a 16 x sizeof(float)-nyi adatot

			// kapcsoljuk be a VAO-t (a VBO j�n vele egy�tt)
			glBindVertexArray(m_vaoID);

			// kirajzol�s
			glDrawElements(GL_TRIANGLES,		// primit�v t�pus
				m_cylinder_indices.size(),		// hany csucspontot hasznalunk a kirajzolashoz
				GL_UNSIGNED_SHORT,	// indexek tipusa
				0);
		}

		{
			//Y tengely
			m_matWorld = glm::scale(glm::vec3(0.06f, 2.0f, 0.06f));

			glm::mat4 mvp = m_matProj * m_matView * m_matWorld;

			// majd k�ldj�k �t a megfelel� m�trixot!
			glUniformMatrix4fv(m_loc_mvp,// erre a helyre t�lts�nk �t adatot
				1,			// egy darab m�trixot
				GL_FALSE,	// NEM transzpon�lva
				&(mvp[0][0])); // innen olvasva a 16 x sizeof(float)-nyi adatot

			// kapcsoljuk be a VAO-t (a VBO j�n vele egy�tt)
			glBindVertexArray(m_vaoID);

			// kirajzol�s
			glDrawElements(GL_TRIANGLES,		// primit�v t�pus
				m_cylinder_indices.size(),		// hany csucspontot hasznalunk a kirajzolashoz
				GL_UNSIGNED_SHORT,	// indexek tipusa
				0);
		}

		{
			//Z tengely
			m_matWorld = glm::translate(glm::vec3(0.0f, 0.0f, 3.0f)) * glm::scale(glm::vec3(0.06f, 0.04f, 3.0f));

			glm::mat4 mvp = m_matProj * m_matView * m_matWorld;

			// majd k�ldj�k �t a megfelel� m�trixot!
			glUniformMatrix4fv(m_loc_mvp,// erre a helyre t�lts�nk �t adatot
				1,			// egy darab m�trixot
				GL_FALSE,	// NEM transzpon�lva
				&(mvp[0][0])); // innen olvasva a 16 x sizeof(float)-nyi adatot

			// kapcsoljuk be a VAO-t (a VBO j�n vele egy�tt)
			glBindVertexArray(m_vaoID);

			// kirajzol�s
			glDrawElements(GL_TRIANGLES,		// primit�v t�pus
				m_cylinder_indices.size(),		// hany csucspontot hasznalunk a kirajzolashoz
				GL_UNSIGNED_SHORT,	// indexek tipusa
				0);
		}
	}
	*/

	// VAO kikapcsolasa
	glBindVertexArray(0);

	// shader kikapcsolasa
	glUseProgram( 0 );
}

void CMyApp::KeyboardDown(SDL_KeyboardEvent& key)
{
	switch (key.keysym.sym) {
	case SDLK_1:
		m_key = 1.0f;
		m_curr_col = glm::vec3(1.0f, 0.0f, 0.0f);
		break;
	case SDLK_2:
		m_key = 1.0f;
		m_curr_col = glm::vec3(0.0f, 1.0f, 0.0f);
		break;
	case SDLK_3:
		m_key = 1.0f;
		m_curr_col = glm::vec3(0.0f, 0.0f, 1.0f);
		break;
	}
}

void CMyApp::KeyboardUp(SDL_KeyboardEvent& key)
{
}

void CMyApp::MouseMove(SDL_MouseMotionEvent& mouse)
{

}

void CMyApp::MouseDown(SDL_MouseButtonEvent& mouse)
{
}

void CMyApp::MouseUp(SDL_MouseButtonEvent& mouse)
{
}

void CMyApp::MouseWheel(SDL_MouseWheelEvent& wheel)
{
}

// a k�t param�terbe az �j ablakm�ret sz�less�ge (_w) �s magass�ga (_h) tal�lhat�
void CMyApp::Resize(int _w, int _h)
{
	glViewport(0, 0, _w, _h);

	m_matProj = glm::perspective(  45.0f,		// 90 fokos nyilasszog
									_w/(float)_h,	// ablakmereteknek megfelelo nezeti arany
									0.01f,			// kozeli vagosik
									100.0f);		// tavoli vagosik
}