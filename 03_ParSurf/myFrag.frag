#version 130

in vec3 vs_out_col;
in vec3 vs_out_pos;

out vec4 fs_out_col;

uniform vec3 curr_col;
uniform float f;
uniform float key;

void main()
{
	if (key == 1)
	{
		fs_out_col = vec4((f * curr_col + (1.0 - f) * vs_out_col), 1);
	}
	else
	{
		fs_out_col = vec4(vs_out_col, 1);
	}
}
